<?php

/*
	Template Name: Homepage
*/

get_header(); ?>

	<section class="registration-form">

		<div class="section-header">
			<div class="logo">
				<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="headline">
				<h2><?php the_field('headline'); ?></h2>
			</div>

			<div class="copy">
				<?php the_field('deck'); ?>
			</div>			
		</div>
		
		<form id="zoom-registration" novalidate method="post">
			<div class="field first-name">
				<label for="first-name">First Name <span class="req">*</span></label>
				<input type="text" name="first-name" required />
			</div>

			<div class="field last-name">
				<label for="last-name">Last Name <span class="req">*</span></label>
				<input type="text" name="last-name" required />
			</div>

			<div class="field email">
				<label for="email">Email <span class="req">*</span></label>
				<input type="email" name="email" required data-pristine-required-message="A valid email address is required" />
			</div>

			<div class="field options">
				<label class="options-label">Choose Workshop Date <span class="req">*</span></label>

				<?php if(have_rows('options')): while(have_rows('options')): the_row(); ?>
 
					<div class="option">
						<input type="checkbox" name="options" id="<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>" value="<?php the_sub_field('occurence_id'); ?>" required />
						<label for="<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>"><?php the_sub_field('label'); ?></label>					
					</div>

				<?php endwhile; endif; ?>
			</div>
			
			<div class="field submit">
				<input type="submit" value="Register" id="register-trigger" />				
			</div>
		</form>


	</section>

<?php get_footer(); ?>