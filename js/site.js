// Submit meeting registration
window.onload = function () {

    var form = document.getElementById("zoom-registration");

    // create the pristine instance
    var pristine = new Pristine(form, {
		classTo: 'field',
		errorClass: 'has-danger',
		successClass: 'has-success',
		errorTextParent: 'field',
		errorTextTag: 'div',
		errorTextClass: 'text-help'
	}, false);

    form.addEventListener('submit', function (e) {
       e.preventDefault();
       
       // check if the form is valid
       var valid = pristine.validate(); // returns true or false

       if(valid === true) {

       		// Get user information
			var data = JSON.stringify({
				"email": document.querySelector('#zoom-registration [name="email"]').value,
				"first_name": document.querySelector('#zoom-registration [name="first-name"]').value,
				"last_name": document.querySelector('#zoom-registration [name="last-name"]').value
			});

			// Checked meeting optons
			var options = document.querySelectorAll('#zoom-registration [name="options"]:checked');
			var occurenceIDs = "";

			// Meeting options into comma-separated values string
			for (var i = 0; options[i]; i++) {
			    if (options[i].checked) {
			        occurenceIDs += "," + options[i].value;
			    }
			}

			// Remove first comma front front of string
			if (occurenceIDs) {
				occurenceIDs = occurenceIDs.substring(1);
			}

			// Begin XMLHttpRequest (code from/adapted from Zoom API documentation -- https://marketplace.zoom.us/docs/api-reference/zoom-api/webinars/webinarregistrantcreate)
			var xhr = new XMLHttpRequest();
			xhr.withCredentials = true;

			xhr.addEventListener("readystatechange", function () {
				if (this.readyState === this.DONE) {
				console.log(this.responseText);
				}
			});

			// Append Occurence IDs
			xhr.open("POST", "https://api.zoom.us/v2/webinars/83526458066/registrants?occurence_ids=" + occurenceIDs);
			xhr.setRequestHeader("content-type", "application/json");
			xhr.setRequestHeader("authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOm51bGwsImlzcyI6ImFvTzY3YUVnVExTRTN6U0RMUDBrSXciLCJleHAiOjE1OTU5NjYzMTUsImlhdCI6MTU5NTk2MDkxNX0.BKF8m-4EkjZcksZ1XznFL5n3-jheJHlB1wi3cM4OnUM");

			xhr.send(data);
       }
    });
};