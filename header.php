<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	
	<header class="site-header grid">

		<div class="site-logo">
			<h1><?php the_field('site_name', 'options'); ?></h1>
			<h3><?php the_field('site_tagline', 'options'); ?></h3>
		</div>

		<div class="site-description">
			<p><?php the_field('site_description', 'options'); ?></p>
		</div>

	</header>

	<main class="site-content grid">	